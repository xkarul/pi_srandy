import RPi.GPIO as GPIO
import cv2, time

time_interval = 3

GPIO.setmode(GPIO.BOARD)
GPIO.setup(3, GPIO.OUT)

detect = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
cam = cv2.VideoCapture(2)

last_face_time = time.time()
while True:
    check, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = detect.detectMultiScale(gray, 1.1, 5)
    if faces != ():
        last_face_time = time.time()
        GPIO.output(3, GPIO.HIGH)
    if time.time() - last_face_time > time_interval:
        GPIO.output(3, GPIO.LOW)

cam.release()
